module gitlab.com/gitlab-org/security-products/analyzers/command/v2

go 1.15

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.8.0
	github.com/urfave/cli/v2 v2.4.4
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v4 v4.1.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
)
