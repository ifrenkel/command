package command

import (
	"fmt"
	"runtime"

	"github.com/urfave/cli/v2"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/search"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

var (
	// appSecDocsURL refers to troubleshooting section of Application Security Docs page
	appSecDocsURL = "https://docs.gitlab.com/ee/user/application_security/#secure-job-failing-with-exit-code-1"
)

// Config struct describes the required implementation details an analyzer
// must provide to generate appropriate CLI commands for it to expose.
type Config struct {
	Analyzer            report.AnalyzerDetails // Details about the analyzer which wraps the scanner
	ArtifactName        string                 // Name of the generated artifact
	Match               search.MatchFunc       // Match is a function that detects a compatible project.
	Analyze             AnalyzeFunc            // Analyze is a function that performs the analysis where a project was detected.
	AnalyzeFlags        []cli.Flag             // AnalyzeFlags is a set command line options used by the analyze function (optional).
	AnalyzeAll          bool                   // AnalyzeAll instructs the run command to analyze the root directory (false by default).
	Convert             ConvertFunc            // Convert is a function that turns the analyzer output into a compatible artifact.
	CACertImportOptions cacert.ImportOptions   // CACertImportOptions are options for the import of CA certificates.
	Scanner             report.ScannerDetails  // Scanner contains detailed information about the scanner
	ScanType            report.Category        // ScanType is the type of the scan (container_scanning, dependency_scanning, dast, sast)
	Serializer          SerializerFunc         // Serializer implements a function for serializing and optimizing report output
}

// NewCommands function creates a slice of CLI command structs
// that contains all required analyzer commands: run, search, analyze, convert.
func NewCommands(cfg Config) []*cli.Command {
	if cfg.Match == nil {
		panic("Match function not defined")
	}
	if cfg.Analyze == nil {
		panic("Analyze function not defined")
	}
	if cfg.AnalyzeFlags == nil {
		panic("Analyze flags not defined")
	}
	if cfg.Convert == nil {
		panic("Convert function not defined")
	}

	return []*cli.Command{
		Run(cfg),
		Search(cfg),
		Analyze(cfg),
		Convert(cfg),
	}
}

// NewApp creates a new cli app with the given details describing the analyzer
func NewApp(analyzer report.AnalyzerDetails) *cli.App {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = fmt.Sprintf("%s (%s %s/%s)", analyzer.Version, runtime.Version(), runtime.GOOS, runtime.GOARCH)
	app.Authors = []*cli.Author{{Name: analyzer.Vendor.Name}}
	app.Usage = "analyze a directory and generate a security report"
	app.ExitErrHandler = func(context *cli.Context, err error) {
		if err != nil && log.GetLevel() <= log.InfoLevel {
			log.Errorf(errOnExitMsg(err))
		}
	}

	log.SetFormatter(&logutil.Formatter{Project: analyzer.Name})
	log.Info(analyzer)

	return app
}

// errOnExitMsg is the default log message that gets printed when the analyzer
// exits with an error and the log level is set to `logrus.InfoLevel` or lower
func errOnExitMsg(err error) string {
	return fmt.Sprintf(`Analyzer has exited with error: '%s'. To debug, set 'SECURE_LOG_LEVEL' CI variable to "debug". See %s for more details.`, err, appSecDocsURL)
}
