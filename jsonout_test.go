package command

import (
	"io/fs"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func TestSerialize(t *testing.T) {
	// test cases
	tcs := []struct {
		name        string
		indented    bool
		expectation string
	}{
		{
			"Indented",
			true,
			`{
  "version": "0.0.0",
  "vulnerabilities": null,
  "dependency_files": null,
  "scan": {
    "analyzer": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "scanner": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "type": ""
  }
}
`,
		},
		{
			"Not Indented",
			false,
			`{"version":"0.0.0","vulnerabilities":null,"dependency_files":null,"scan":{"analyzer":{"id":"","name":"","vendor":{"name":""},"version":""},"scanner":{"id":"","name":"","vendor":{"name":""},"version":""},"type":""}}
`,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			r := report.Report{}

			t.Run("SerializeJSONToFile", func(t *testing.T) {
				jsonfile, err := os.CreateTemp("/tmp", "jsonfile")
				require.NoError(t, err)
				// Store the filename but delete the temp file immediately because we want
				// SerializeJSONToFile to work as it would in a real run (it creates the
				// artifact rather than being provided with an empty file).
				jsonfileName := jsonfile.Name()
				require.NoError(t, os.Remove(jsonfileName))

				err = SerializeJSONToFile(&r, jsonfileName, "testdata/search", tc.indented, false)
				require.NoError(t, err, "cannot serialize JSON")

				content, err := ioutil.ReadFile(jsonfile.Name())
				require.NoError(t, err)

				require.Equal(t, tc.expectation, string(content))

				info, err := os.Stat(jsonfileName)
				require.NoError(t, err)
				assert.Equal(t, fs.FileMode(0644), info.Mode().Perm())
			})

			t.Run("SerializeJSONToWriter", func(t *testing.T) {
				writer := new(strings.Builder)

				err := SerializeJSONToWriter(&r, writer, "testdata/search", tc.indented, false)
				require.NoError(t, err, "cannot serialize JSON")

				require.Equal(t, tc.expectation, writer.String())
			})
		})
	}
}

func TestEliminateRedundancies(t *testing.T) {
	testinput := &report.Report{
		Version: report.Version{},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              &report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 1000,
					LineEnd:   0,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              &report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 4,
					LineEnd:   4,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              &report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 4,
					LineEnd:   5,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              &report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/not-existent",
					LineStart: 4,
					LineEnd:   5,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
		},
		Remediations:    []report.Remediation{},
		DependencyFiles: []report.DependencyFile{},
		Scan:            report.Scan{},
		Analyzer:        "",
		Config:          ruleset.Config{},
	}

	expectation := &report.Report{
		Version: report.Version{},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              &report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 1000,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              &report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 4,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              &report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 4,
					LineEnd:   5,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
		},
		Remediations:    []report.Remediation{},
		DependencyFiles: []report.DependencyFile{},
		Scan:            report.Scan{},
		Analyzer:        "",
		Config:          ruleset.Config{},
	}

	optimized, err := EliminateRedundancies(testinput, "testdata/search")
	require.NoError(t, err, "cannot run optimizations")
	require.Equal(t, optimized, expectation)
}
